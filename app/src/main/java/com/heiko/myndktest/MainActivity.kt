package com.heiko.myndktest

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import com.heiko.myndktest.databinding.ActivityMainBinding
import java.util.Arrays

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        // Example of a call to a native method
        //binding.sampleText.text = stringFromJNI()

        val map = test1()
        for (entry in map.entries) {
            Log.i("ZZZZ", "key:${entry.key} value:${entry.value}")
        }

        val map2 = test2()
        for (entry in map2.entries) {
            Log.i("ZZZZ", "key:${entry.key}")
            var str = ""
            for (i in entry.value) {
                str+=" $i"
            }
            Log.i("ZZZZ","value:$str")
        }

        val result = test3()
        val s =
            "myPublicInt:${result.myPublicInt} myPublicFloat:${result.myPublicFloat} myPublicString:${result.myPublicString} floatValue:${result.floatValue} stringValue:${result.stringValue} floatArray:${
                Arrays.toString(result.myPublicFloatArray)
            }"
        binding.sampleText.text = s

        Log.i("ZZZZ", "拼接:$s")

        val resultArray = test4()
        Log.i("ZZZZ", "resultArray.length:" + resultArray.size)
        for (javaBean in resultArray) {
            Log.i(
                "ZZZZ",
                "myPublicInt:" + javaBean.myPublicInt + " stringValue:" + javaBean.stringValue
            )
        }

        val array = test5()
        Log.i("ZZZZ", "二维数组长度:" + array.size)
        for (i in 0 until array.size) {
            val childSize = array[i].size
            var str = ""
            for (j in 0 until childSize) {
                str += " ${array[i][j]}"
            }

            Log.i("ZZZZ", "二维数组:${str}")
        }
    }

    /**
     * A native method that is implemented by the 'myndktest' native library,
     * which is packaged with this application.
     */
    external fun stringFromJNI(): String

    external fun test1(): Map<String, String>

    external fun test2(): Map<String, Array<Int>>

    external fun test3(): JavaBean

    external fun test4(): Array<JavaBean>

    external fun test5(): Array<IntArray>

    companion object {
        // Used to load the 'myndktest' library on application startup.
        init {
            System.loadLibrary("myndktest")
        }
    }
}