package com.heiko.myndktest

interface MyTest {
    //int[] t1();
    fun t1(): IntArray?
    // int[][] t2();
    fun t2(): Array<IntArray?>?
    // Map<String, String> t3();
    fun t3(): Map<String?, String?>?
    // Map<String, Integer[]> t4();
    fun t4(): Map<String?, Array<Int?>?>?
    //Integer[] t5();
    fun t5(): Array<Int?>?
}
