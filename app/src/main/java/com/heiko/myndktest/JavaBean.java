package com.heiko.myndktest;

public class JavaBean {
    public int myPublicInt = 1;
    public float myPublicFloat = 2.35F;
    public String myPublicString = "hello";

    public int[] myPublicIntArray = new int[]{1, 2, 3, 4, 5, 6, 7, 8, 9};
    public float[] myPublicFloatArray = new float[]{1.2F, 2.3F, 3.4F,4.5F};
    public String[] myPublicStringArray = new String[]{"hello", "world", "!"};

    public JavaBean() {
    }

    private int intValue = 77;
    private float floatValue = 9.788F;
    private String stringValue = "world";

    public int getIntValue() {
        return intValue;
    }

    public void setIntValue(int intValue) {
        this.intValue = intValue;
    }

    public void setFloatValue(float value) {
        this.floatValue = value;
    }

    public float getFloatValue() {
        return floatValue;
    }

    public String getStringValue() {
        return stringValue;
    }

    public void setStringValue(String stringValue) {
        this.stringValue = stringValue;
    }
}
